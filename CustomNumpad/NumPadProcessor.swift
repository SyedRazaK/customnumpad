//
//  NumPadProcessor.swift
//  CustomNumPad
//
//  Created by Hussnain Raza on 12/20/15.
//  Copyright © 2015 5SigmaSoft, Inc. All rights reserved.
//

import UIKit

class NumPadProcessor{

    
    var maxLength = 10 {
        didSet {
                previousOperand = resetOperand()
                currentOperand = resetOperand()
        }
    }
    
    
    var maxFractionDigits = 2{
        didSet {
            previousOperand = resetOperand()
            currentOperand = resetOperand()
        }
    }
    var automaticDecimal = true {
        didSet {
            if automaticDecimal {
                previousOperand = resetOperand()
                currentOperand = resetOperand()
            }
        }
    }
    var previousOperand: String = "0"
    var currentOperand: String = "0"
    var storedOperator: NumPadKey?
    var decimalDigit = 0
    
 
    func storeOperand(value: Int) -> String {
        let operand = "\(value)"
        if currentOperand == "0" {
            currentOperand = operand
        }
        else
        {
            if currentOperand.characters.count <= self.maxLength{
                if  operand == "10" {
                    currentOperand += "00"
                }else
                {
                    
                    currentOperand += operand
                }
            }
        }
        
//        if storedOperator == .Equal {
//            if automaticDecimal {
//                currentOperand = previousOperand + operand
//            } else {
//                currentOperand = operand
//            }
//            storedOperator = nil
//        }
        
        if automaticDecimal {
            currentOperand = currentOperand.stringByReplacingOccurrencesOfString(decimalSymbol(), withString: "")
                if currentOperand[currentOperand.startIndex] == "0" {
                    currentOperand.removeAtIndex(currentOperand.startIndex)
                }
                let char = decimalSymbol()[decimalSymbol().startIndex]
                currentOperand.insert(char, atIndex: currentOperand.endIndex.advancedBy(-maxFractionDigits))
            
        }
        
        return currentOperand
    }
    
    func deleteLastDigit() -> String {
        if currentOperand.characters.count > 1 {
            currentOperand.removeAtIndex(currentOperand.endIndex.predecessor())
            
            if automaticDecimal {
                currentOperand = currentOperand.stringByReplacingOccurrencesOfString(decimalSymbol(), withString: "")
                if currentOperand.characters.count < maxFractionDigits + 1 {
                    currentOperand.insert("0", atIndex: currentOperand.startIndex)
                }
                let char = decimalSymbol()[decimalSymbol().startIndex]
                currentOperand.insert(char, atIndex: currentOperand.endIndex.advancedBy(-maxFractionDigits))
            }
        } else {
            currentOperand = resetOperand()
        }
        
        return currentOperand
    }
    
    
    
    
    func clearAll() -> String {
        storedOperator = nil
        currentOperand = resetOperand()
        previousOperand = resetOperand()
        return currentOperand
    }
    
    private func decimalSymbol() -> String {
        return "."
    }
    
    private func resetOperand() -> String {
        var operand = "0"
        if automaticDecimal {
            operand = convertOperandToDecimals(operand)
        }
        return operand
    }
    
    
    private func convertOperandToDecimals(operand: String) -> String {
        var fractions = "."
        for _  in 1...maxFractionDigits{
            fractions += "0"
        }
        return operand + fractions
    }
    
    private func formatValue(value: Double) -> String {
        var raw = "\(value)"
        if automaticDecimal {
            raw = String(format: "%.2f", value)
            return raw
        } else {
            var end = raw.endIndex.predecessor()
            var foundDecimal = false
            while end != raw.startIndex && (raw[end] == "0" || isDecimal(raw[end])) && !foundDecimal {
                foundDecimal = isDecimal(raw[end])
                raw.removeAtIndex(end)
                end = end.predecessor()
            }
            return raw
        }
    }
    
    private func isDecimal(char: Character) -> Bool {
        return String(char) == decimalSymbol()
    }

    
}