//
//  CustomNumPad.swift
//  CustomNumPad
//
//  Created by Hussnain Raza on 12/20/15.
//  Copyright © 2015 5SigmaSoft, Inc. All rights reserved.
//

import UIKit
import AudioToolbox
public protocol CustomNumpadDelegate: class {
    func numpad(NumPad: CustomNumPad, didChangeValue value: String)
}

enum NumPadKey: Int {
    case Zero = 1
    case One
    case Two
    case Three
    case Four
    case Five
    case Six
    case Seven
    case Eight
    case Nine
    case DoubleZero
    case Clear
    case Next
    case Hide
}

public class CustomNumPad: UIView{
    
    
    public static let Next = "Next"
    public static let Hide = "Hide"
    public weak var delegate: CustomNumpadDelegate?
    
    public var numbersBackgroundColor = UIColor(white: 0.97, alpha: 1.0) {
        didSet {
            adjustLayout()
        }
    }
    
    public var numbersTextColor = UIColor.blackColor() {
        didSet {
            adjustLayout()
        }
    }
    
    public var operationsBackgroundColor = UIColor(white: 0.75, alpha: 1.0) {
        didSet {
            adjustLayout()
        }
    }
    
    public var operationsTextColor = UIColor.whiteColor() {
        didSet {
            adjustLayout()
        }
    }
    
    public var hideBackgroundColor = UIColor(red:0.96, green:0.5, blue:0, alpha:1) {
        didSet {
            adjustLayout()
        }
    }
    public var hideTextColor = UIColor.whiteColor() {
        didSet {
            adjustLayout()
        }
    }
    
    public var isDecimal = true {
        didSet {
            processor.automaticDecimal = isDecimal
            adjustLayout()
        }
    }
    
    public var maxLength = 10{
        didSet {
            processor.maxLength = maxLength
            adjustLayout()
        }
    }
    
    public var fractionDigits = 2{
        didSet {
            if fractionDigits < 1 {
                processor.maxFractionDigits = 1
            }
            else
            {
                processor.maxFractionDigits = fractionDigits
            }
            
            adjustLayout()
        }
    }
    
    public var isAlertSound = true{
        didSet {
            adjustLayout()
        }
    }
    
    var view: UIView!
    private var processor = NumPadProcessor()
    
   // @IBOutlet weak var zeroDistanceConstraint: NSLayoutConstraint!
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXib()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        loadXib()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        adjustLayout()
    }
    
    private func loadXib() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        adjustLayout()
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CustomNumPad", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        adjustButtonConstraint()
        return view
    }
    
    private func adjustLayout() {
//        if viewWithTag(NumPadKey.Decimal.rawValue) != nil {
//            adjustButtonConstraint()
//        }
        
        for var i = 1; i <= NumPadKey.DoubleZero.rawValue; i++ {
            if let button = self.view.viewWithTag(i) as? UIButton {
                button.tintColor = numbersBackgroundColor
                button.setTitleColor(numbersTextColor, forState: .Normal)
            }
        }
        
        for var i = NumPadKey.Clear.rawValue; i <= NumPadKey.Next.rawValue; i++ {
            if let button = self.view.viewWithTag(i) as? UIButton {
                button.tintColor = operationsBackgroundColor
                button.setTitleColor(operationsTextColor, forState: .Normal)
                button.tintColor = operationsTextColor
            }
        }
        
        if let button = self.view.viewWithTag(NumPadKey.Hide.rawValue) as? UIButton {
            button.tintColor = hideBackgroundColor
            button.setTitleColor(hideTextColor, forState: .Normal)
        }
    }
    
    private func adjustButtonConstraint() {
        //let width = UIScreen.mainScreen().bounds.width / 4.0
        //zeroDistanceConstraint.constant = false ? width + 2.0 : 1.0
        layoutIfNeeded()
    }
    
    @IBAction func buttonPressed(sender: UIButton) {
        if (self.isAlertSound){
            AudioServicesPlaySystemSound(1104)
        }
        switch (sender.tag) {
        case (NumPadKey.Zero.rawValue)...(NumPadKey.DoubleZero.rawValue):
            let output = processor.storeOperand(sender.tag-1)
            delegate?.numpad(self, didChangeValue: output)
//        case NumPadKey.Decimal.rawValue:
//            let output = processor.addDecimal()
//            delegate?.numpad(self, didChangeValue: output)
//        case NumPadKey.Clear.rawValue:
//            let output = processor.clearAll()
//            delegate?.numpad(self, didChangeValue: output)
        case NumPadKey.Clear.rawValue:
            let output = processor.deleteLastDigit()
            delegate?.numpad(self, didChangeValue: output)
        case NumPadKey.Next.rawValue:
            delegate?.numpad(self, didChangeValue: "Next")
        case NumPadKey.Hide.rawValue:
            delegate?.numpad(self, didChangeValue: "Hide")
            break
        default:
            break
        }
    }
    
}